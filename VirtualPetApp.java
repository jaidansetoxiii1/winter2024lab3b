import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args) { 
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Hyena[] cackle = new Hyena[4];
		
		for (int i = 0; i < cackle.length; i++) {
			cackle[i] = new Hyena();
			
			System.out.println("What is Hyena " + (i + 1) + "'s Size? (Double)");
			cackle[i].size = reader.nextDouble();
			
			System.out.println("What is Hyena " + (i + 1) + "'s Agility? (Int)");
			cackle[i].agility = reader.nextInt();
			
			System.out.println("What is Hyena " + (i + 1) + "'s Wit? (Int)");
			cackle[i].wit = reader.nextInt();
			
		}
		
		System.out.println("Stats of Hyena 4: " + cackle[cackle.length - 1].size + "lbs. , " + cackle[cackle.length - 1].agility + " agility, " + cackle[cackle.length - 1].wit + " wit");
		
		
		cackle[0].search();
		cackle[0].maul();
	}
}