import java.util.Scanner;
import java.util.Random;

public class Hyena {
	Random rand = new Random();
	
	public double size;
	public int agility;
	public int wit;
	
	public void search(){
		int result = rand.nextInt(20);
		
		System.out.println("Searching...");
		System.out.println("You rolled a " + result + " + " + wit + "(wit)");
		result += wit;
		
		System.out.println("Result: " + result);
		
		if (result >= 10) {
			System.out.println("Success!!");
		}
		else {
			System.out.println("Nothing found...");
			
		}
		
	}
	
	public void maul(){
		System.out.println("Your hyena chomps down.");
	}
}